class Counter{
    static #count = Number(0);
    // Uncomment the below code to test error log
    //  static #count = Number(-1); 
    static get count(){
        return this.#count;
    }
    static increase(){
        this.#count++;
    }
    static reset(){
        this.#count = Number(0);
    }

}
module.exports = Counter;