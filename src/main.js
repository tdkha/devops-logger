// Generate template for Express app
const express = require('express');
const app = express();
const PORT = 3000;
const HOSTNAME = '127.0.0.1';
const routes = require('./routes');
const logger = require('./logger')
app.use('/', routes);
logger.info("[MAIN] Starting");
process.on ('SIGTERM', () =>{
    logger.info("[MAIN] Stopping");
    process.exit()
}) ;
process.on ('SIGINT',() =>{
    logger.info("[MAIN] Stopping");
    process.exit()
}) ;
app.listen(PORT,HOSTNAME, () =>{
    console.log(`Example app listening on port http://${HOSTNAME}:${PORT}`)
});
