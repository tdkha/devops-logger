const express = require("express");
const router = express.Router();
const Counter = require("./counter");
const logger = require("./logger");

router.get("/", (req, res) => {
  try {
    logger.info("[ENDPOINT] GET '/'");
    res.status(200).send("Hello Counter!");
  } catch (err) {
    logger.error(err.message); // Log the error
    res.status(500).send("Error occured");
  }
});

router.get("/counter-increase", (req, res) => {
  try {
    const counter = Counter.count;
    if (counter < 0) {
      Counter.reset();
      throw new Error("Invalid counter value. Force reset");
    }
    Counter.increase();
    logger.info(`[ENDPOINT] GET /counter-increase`);
    logger.info(`[COUNTER] increase ${Counter.count} (after the increase)`);
    res.status(200).send(`Counter: ${Counter.count}`);
  } catch (err) {
    logger.error(err.message); // Log the error
    if (err.message == "Invalid counter value. Force reset") {
      res.status(500).send(err.message);
    }
    res.status(500).send("Error occured");
  }
});
router.get("/counter-read", (req, res) => {
  try {
    const counter = Counter.count;
    if (counter < 0) {
      Counter.reset();
      throw new Error("Invalid counter value. Force reset");
    }
    logger.info(`[ENDPOINT] GET /counter-read`);
    logger.info(`[COUNTER] read ${counter}`);
    res.status(200).send(`Counter: ${Counter.count}`);
  } catch (err) {
    logger.error(err.message); // Log the error
    if (err.message == "Invalid counter value. Force reset") {
      res.status(500).send(err.message);
    }
    res.status(500).send("Error occured");
  }
});
router.get("/counter-reset", (req, res) => {
  try {
    logger.info(`[ENDPOINT] GET /counter-reset`);
    Counter.reset();
    logger.info(`[COUNTER] zeroed 0`);
    res.status(200).send(`Counter: ${Counter.count}`);
  } catch (err) {
    logger.error(err.message); // Log the error
    res.status(500).send("Error occured");
  }
});

module.exports = router;
